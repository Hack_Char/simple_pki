from app import app
from flask import send_from_directory, request
import sys
from simple_pki import simple_pki

@app.route('/')
@app.route('/ca.crt')
def index():
    return send_from_directory("..","ca.crt")

@app.route('/csr', methods=['POST'])
def sign():

    CA = simple_pki()
    CA.load_key('ca.key')
    if not CA.keyLoaded():
        CA.gen_key()
        CA.save_key('ca.key')
    CA.load_crt('ca.crt')
    if not CA.crtLoaded():
        CA.self_sign(cn="CA", is_CA=True)
        CA.save_crt('ca.crt')

    return(CA.ca_sign(request.form['csr']))
    