Simple Python PKI Setup /w Remote CA
====================================
WARNING! This is a work in progress!

It currently will work, but there is zero security.

This is an attempt to make generating a PKI set up quickly with few resources.

See python module serverPKI for a more robust solution.

Usage
=====
Install using
::
  pipenv install

(Requires pip3 python3 and pipenv)

Run the flask debug server with 
::
  pipenv run flask run

Use
::
  ./gencert . example.com localhost:5000
to create a certificate for example.com in the current directory


Since the server is running from this directory also, you'll see the ca.crt and ca.key show up.

Can be run from other directories, etc.

Theory Of Operation
===================
Remove flask daemon will return the CA certifacte at localhost:5000

Sending a post request to localhost:5000/csr with a PEM encoded CSR returns a signed certificate

To Do
=====
* Add TLS using CA signed certificate
* Add pre-shared secret for certificate generation (static? JWT?)
* Make loading the CA certificate and key more efficient server side
* Prefer to avoid databases and keep
