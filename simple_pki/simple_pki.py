#!/usr/bin/env python3
"""
Class to make Simple PKI Calls
Using python cryptography library

see https://github.com/pyca/cryptography

"""
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
import datetime
import os
import sys
import requests

DEFAULT_PASSPHRASE="password"
DEFAULT_ORG="My Simple PKI"

class simple_pki:

    def __init__(self, passphrase=DEFAULT_PASSPHRASE, org=DEFAULT_ORG):
        self.key=None
        self.crt=None
        self.csr=None
        self.passphrase=passphrase
        self.org=org
        self.is_CA=False

    def load_key(self, filename):
        if not os.path.isfile(filename):
            return
        try:
            with open(filename,'rb') as f:
                d = f.read()
            self.key = serialization.load_pem_private_key(
                        data=d,
                        password=self.passphrase.encode(),
                        backend=default_backend()
                        )
        except Exception as e:
            print(e)

    def keyLoaded(self):
        return not self.key == None

    def crtLoaded(self):
        return not self.crt == None

    def gen_key(self):
        self.key=rsa.generate_private_key(
            public_exponent=65537,
            key_size=4096,
            backend=default_backend()
        )

    def save_key(self, filename):
        try:
            with open(filename,'wb') as f:
                f.write(self.key.private_bytes(
                    encoding=serialization.Encoding.PEM,
                    format=serialization.PrivateFormat.TraditionalOpenSSL,
                    encryption_algorithm=serialization.BestAvailableEncryption(self.passphrase.encode())
                ))
        except Exception as e:
            print(e)
            os.remove(filename)

    def gen_csr(self, cn=""):
        if cn=="":
            cn = self.org
        self.csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
                x509.NameAttribute(NameOID.ORGANIZATION_NAME, self.org),
                x509.NameAttribute(NameOID.COMMON_NAME, cn),
                ]
            )).sign(self.key, hashes.SHA256(), default_backend())


    def self_sign(self, cn="", is_CA=False):
        if cn=="":
            cn = self.org
        subject = issuer = x509.Name([
            x509.NameAttribute(NameOID.ORGANIZATION_NAME, self.org),
            x509.NameAttribute(NameOID.COMMON_NAME, cn),
        ])
        certbuild = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            self.key.public_key()
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=365)
        )

        if is_CA:
            certbuild=certbuild.add_extension(
                x509.BasicConstraints(ca=True, path_length=0),
                critical=True
            ).add_extension(
                x509.KeyUsage(
                    digital_signature=True,
                    content_commitment=False,
                    key_encipherment=True,
                    data_encipherment=True,
                    key_agreement=True,
                    key_cert_sign=True,
                    crl_sign=True,
                    encipher_only=False,
                    decipher_only=False
                ),
                critical=True
            )
            self.is_CA = True

        self.crt=certbuild.sign(self.key, hashes.SHA256(), default_backend())

    def save_crt(self, filename):
        try:
            with open(filename,'wb') as f:
                f.write(self.crt.public_bytes(serialization.Encoding.PEM))
        except Exception as e:
            print(e)
            os.remove(filename)

    def read_csr(self, csr):
        self.csr = x509.load_der_x509_csr(csr, default_backend())

    def ca_sign(self, csr_text):
        #if not self.is_CA:
        #    print("Error! This is not a CA")
        #    return "Error!"

        #print(csr_text, file=sys.stderr)

        csr=x509.load_pem_x509_csr(csr_text.encode(), default_backend())

        subject = csr.subject
        issuer = self.crt.subject

        certbuild = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            csr.public_key()
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=365)
        )

        ret_crt = certbuild.sign(self.key, hashes.SHA256(), default_backend())
        return ret_crt.public_bytes(serialization.Encoding.PEM)


    def load_crt(self, filename):
        if not os.path.isfile(filename):
            return
        try:
            with open(filename,'rb') as f:
                d=f.read()
            self.crt = x509.load_pem_x509_certificate(
                data=d,
                backend=default_backend()
            )
            if x509.BasicConstraints(ca=True, path_length=0) in self.crt.extensions:
                self.is_CA = True

        except Exception as e:
            print(e)
            return

    def remote_sign(self, uri):
        if self.csr is None:
            print("Error! Attempt to request signature of non-existant CSR")
            return
        r = requests.post(uri, data={'csr':self.csr.public_bytes(serialization.Encoding.PEM)})
        #print(r.text)
        self.crt=x509.load_pem_x509_certificate(r.text.encode(), default_backend())
    
        

