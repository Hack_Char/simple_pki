#!/usr/bin/env python3
"""
Generate local key, crt and optionally csr and get it signed remotely
gencert.py [<PATH>] [<NAME>] [<remote URI>]

see https://github.com/pyca/cryptography

"""

from simple_pki import simple_pki
import sys
import os

if __name__ == '__main__':
    try:
        local_path=sys.argv[1]
    except IndexError:
        local_path="."
    try:
        local_name=sys.argv[2]
    except IndexError:
        local_name="pki"
    try:
        remote_pki=sys.argv[3]
    except IndexError:
        remote_pki=None

    local_path=os.path.abspath(local_path)+'/'
    local_key=local_path+local_name+".key"
    local_crt=local_path+local_name+".crt"
    local_csr=local_path+local_name+".csr"

    myPki = simple_pki()
    myPki.load_key(local_key)
    if not myPki.keyLoaded():
        myPki.gen_key()
        myPki.save_key(local_key)

    if remote_pki is None: # self signed cert
        myPki.self_sign()
        myPki.save_crt(local_crt)
        exit(0)

    myPki.gen_csr(local_name)
    myPki.remote_sign('http://localhost:5000/csr')
    myPki.save_crt(local_crt)
